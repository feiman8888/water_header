import React, { Component } from 'react'
import { getSolve, getSolvePage, cleanSolve, fastSolve } from "../../api/request"
import { List, Button, Card, Pagination, notification } from 'antd';
export default class SolveList extends Component {
    constructor() {
        super()
        this.state = {
            total: "",
            data: [],
            nowpage:1
        }
    }
    //通知框
    openNotificationWithIcon = (type, msg) => {
        notification[type]({
            message: msg
        });
    };
    componentDidMount() {
        this.setPage()
        this.getData(1)
    }
    //设置页数
    setPage = () => {
        getSolve().then(res => {
            if (res.data.state) {
                this.setState({
                    total: res.data.data.length / 5 * 10
                })
            }
        }).catch(err => {
            //网络错误
            this.openNotificationWithIcon('error', "网络错误")
        })
    }
    //获取数据
    getData = (page) => {
        getSolvePage({ page }).then(res => {
            if (res.data.state) {
                //获取成功
                this.setState({
                    data: res.data.data
                })
            } else {
                //获取失败
                this.openNotificationWithIcon('error', "数据获取失败，请稍后再试")
            }
        }).catch(err => {
            //网络错误
            this.openNotificationWithIcon('error', "网络错误")
        })
    }
    //撤销事务
    clean=(_id)=>{
        cleanSolve(_id).then(res=>{
            if(res.data.state){
                //撤销事务成功
                this.openNotificationWithIcon('success', "撤销成功!")
                //重新渲染
                this.setPage()
                this.getData(this.state.nowpage)
            }else{
                //撤销失败,请稍后再试
                this.openNotificationWithIcon('error', "撤销失败,请稍后再试!")
            }
        }).catch(err=>{
            //网络错误
            this.openNotificationWithIcon('error', "网络错误")
        })
    }
    //换页
    changePage=(page)=>{
        this.setState({
            nowpage:page
        })
        this.setPage()
        this.getData(page)
    }
    //再次申请
    again=(_id)=>{
        fastSolve({_id}).then(res=>{
            if(res.data.state){
                //获取成功
                //重新渲染数据
                this.getData(this.state.nowpage)
            }
        }).catch(err=>{
            //网络错误
            this.openNotificationWithIcon('error', "网络错误")
        })
    }
    render() {
        let { total } = this.state
        return (
            <div>
                <List
                    itemLayout="horizontal"
                    dataSource={this.state.data}
                    renderItem={item => (
                        <Card title={<h4>申请人:{item.admin}</h4>} extra={item.solveState === "1" ? <Button style={{ backgroundColor: "#73E3F9" }}>已处理</Button> : <div>
                            <Button type="primary" onClick={this.again.bind(this,item._id)}>再次申请</Button>
                            <Button type="primary" danger onClick={this.clean.bind(this, item._id)}>撤销</Button>
                        </div>} style={{ marginBottom: "10px" }}>
                            <List.Item>
                                <List.Item.Meta
                                    size="large"
                                    description={
                                        <div>
                                            <h3>{item.event}</h3>
                                            <div>申请时间:{item.cTime}</div>
                                            {item.solveState === "1" ?<div>处理时间:<span>{item.solveTime}</span></div>:""}
                                            {item.Remarks ? <div style={{marginTop:"15px"}}><span style={{color:"red"}}>处理备注:</span>{item.Remarks}</div>:""}
                                        </div>
                                    }
                                />
                            </List.Item>
                        </Card>
                    )}
                />
                <br />
                <Pagination defaultCurrent={1} total={total} onChange={this.changePage} />
            </div>
        )
    }
}
