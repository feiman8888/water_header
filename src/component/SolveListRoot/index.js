import React, { Component } from 'react'
import { getSolve, solveSolve } from "../../api/request"
import { List, Button, Card, Pagination, notification, Modal } from 'antd';
export default class SolveListRoot extends Component {
    constructor() {
        super()
        this.state = {
            data: [],
            visible:false,
            contents: "",
            _id:""
        }
    }
    //通知框
    openNotificationWithIcon = (type, msg) => {
        notification[type]({
            message: msg
        });
    };
    componentDidMount() {
        this.getData(1)
    }
    //获取数据
    getData = () => {
        getSolve().then(res => {
            if (res.data.state) {
                //获取成功
                this.setState({
                    data: res.data.data.filter(item=>{
                        if(item.solveState==="-1"){
                            return item
                        }
                    })
                })
            } else {
                //获取失败
                this.openNotificationWithIcon('error', "数据获取失败，请稍后再试")
            }
        }).catch(err => {
            //网络错误
            this.openNotificationWithIcon('error', "网络错误")
        })
    }
    //处理事务
    solve = (_id) => {
        this.setState({
            visible: true,
            _id,
        });
    }
    //弹出窗确认按钮
    handleOk = e => {
        //点击了确认按钮
        console.log(this.state.contents);
        let data={}
        if (this.state.contents!==""){
            data.Remarks = this.state.contents
        }
        data._id = this.state._id
        //发请求
        solveSolve(data).then(res=>{
            console.log(res);
            if(res.data.state){
                //处理成功
                //重新拉取数据
                this.getData()
            }else{
                //处理失败
                this.openNotificationWithIcon('error', "处理失败,请稍后再试!")
            }
        }).catch(err=>{
            //网络错误
            this.openNotificationWithIcon('error', "网络错误")
        })
        this.setState({
            visible: false,
        });
    };
    //弹出窗取消按钮
    handleCancel = e => {
        this.setState({
            visible: false,
        });
    };
    //弹出框里面的textarea内容改变出触发函数
    changeRemarks=(e)=>{
        this.setState({
            contents: e.target.value
        })
    }
    render() {
        let { contents } = this.state
        return (
            <div>
                <List
                    itemLayout="horizontal"
                    dataSource={this.state.data}
                    renderItem={item => (
                    <Card title={<h4>申请人:{item.admin}</h4>} extra={<Button type="primary" danger onClick={this.solve.bind(this, item._id)}>处理</Button>} style={{ marginBottom: "10px" }}>
                            <List.Item>
                                <List.Item.Meta
                                    size="large"
                                    description={
                                        <div>
                                            <h3>{item.event}</h3>
                                            <div>申请时间:{item.cTime}</div>
                                            {item.solveState === "1" ? <div>处理时间:<span>{item.solveTime}</span></div> : ""}
                                            {item.Remarks ? <div style={{ marginTop: "15px" }}><span style={{ color: "red" }}>处理备注:</span>{item.Remarks}</div> : ""}
                                        </div>
                                    }
                                />
                            </List.Item>
                        </Card>
                    )}
                />
                <Modal
                    title="处理事务备注(可为空)"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <textarea style={{ width: "450px", height: "100px" }} value={contents} onChange={this.changeRemarks.bind(this)} />
                </Modal>
            </div>
        )
    }
}
