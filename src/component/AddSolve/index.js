import React, { Component } from 'react'
import { Button, Spin, notification } from 'antd';
import { addSolve } from "../../api/request"
export default class AddSolve extends Component {
    constructor() {
        super()
        this.state = {
            contents: "",
            sping: false
        }
    }
    //通知框
    openNotificationWithIcon = (type, msg) => {
        notification[type]({
            message: msg
        });
    };
    change(e) {
        this.setState({
            contents: e.target.value
        })
    }
    onSure = () => {
        this.setState({
            sping: true
        })
        addSolve(this.state.contents).then(res=>{
            console.log(res);
            if(res.data.state){
                //申请成功
                this.openNotificationWithIcon('success', "申请成功")
            }else{
                //申请失败
                this.openNotificationWithIcon('error', "申请失败,请稍后再试!")
            }
        }).catch(err=>{
            //网络错误
            this.openNotificationWithIcon('error', "网络错误")
        }).finally(()=>{
            this.setState({
                sping: false
            })
        })
    }
    render() {
        let { contents, sping } = this.state
        return (
            <div>
                <Spin spinning={sping} style={{ width: "500px" }}>
                    <div>
                        <h4>申请内容:</h4>
                        <textarea style={{ width: "500px", height: "200px" }} value={contents} onChange={this.change.bind(this)} />
                    </div>
                    <br />
                    <Button type="primary" shape="round" onClick={this.onSure}>提交申请</Button>
                </Spin>
            </div>
        )
    }
}
